import java.io.*;
import java.util.*;
class aletoria  {

    public static void main(String[] args) throws IOException {
        
        FileWriter archivoDatos = new FileWriter("datos_generados.txt");
        Random rand = new Random();
        int rango = 301; 
        for (int i = 0; i < 1000; i++) {
            int dato = rand.nextInt(rango);
            archivoDatos.write(Integer.toString(dato) + "\n");
        }
        archivoDatos.close();
        
        FileReader archivo = new FileReader("datos_generados.txt");
        BufferedReader lector = new BufferedReader(archivo);
        ArrayList<Integer> datos = new ArrayList<Integer>();
        String linea;
        while ((linea = lector.readLine()) != null) {
            int dato = Integer.parseInt(linea);
            datos.add(dato);
        }
        lector.close();
        
        int maximo = Collections.max(datos);
        int minimo = Collections.min(datos);
        double promedio = datos.stream().mapToDouble(a -> a).average().orElse(Double.NaN);
        
        Map<Integer, Integer> contador = new HashMap<>();
        for (int i = 0; i < datos.size(); i++) {
            int num = datos.get(i);
            contador.put(num, contador.getOrDefault(num, 0) + 1);
        }
        int moda = Collections.max(contador.entrySet(), Map.Entry.comparingByValue()).getKey();
        
        double sumaCuadrados = datos.stream().mapToDouble(a -> Math.pow((a - promedio), 2)).sum();
        double desviacion = Math.sqrt(sumaCuadrados / datos.size());
        
        FileWriter archivoResultados = new FileWriter("resultados.txt");
        archivoResultados.write("resolviendo del numero  es :"+datos);
        archivoResultados.write("Valor maximo: " + maximo + "\n");
        archivoResultados.write("Valor minimo: " + minimo + "\n");
        archivoResultados.write("Promedio: " + promedio + "\n");
        archivoResultados.write("Moda: " + moda + "\n");
        archivoResultados.write("Desviacion estandar: " + desviacion + "\n");
        archivoResultados.write("\n Autor:Danner_Perez-DNR");
        archivoResultados.close();
    }
}
