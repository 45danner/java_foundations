import java.util.*;

 class Permutaciones {
    
    public static int calcularFactorial(int n) {
        if (n <= 1) {
            return 1;
        } else {
            return n * calcularFactorial(n - 1);
        }
    }
    
    public static int calcularPermutaciones(int n) {
        if (n <= 1) {
            return 1;
        } else {
            return n * calcularPermutaciones(n - 1);
        }
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese la cantidad de elementos de la lista: ");
        int n = sc.nextInt();
        
        int numPermutaciones = calcularPermutaciones(n);
        System.out.println("El numero de permutaciones de " + n + " elementos es: " + numPermutaciones);
    }
}
