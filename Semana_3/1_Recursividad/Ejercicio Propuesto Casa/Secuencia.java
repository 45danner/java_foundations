public class Secuencia {
    public static void main(String[] args) {
        int m = 2;
        int n = 1;
        int resultado = calcularAckermann(m, n);
        System.out.println("El enésimo término de la secuencia de Ackermann para m = " + m + " y n = " + n + " es: " + resultado);
    }
    
    public static int calcularAckermann(int m, int n) {
        if (m == 0) {
            return n + 1;
        } else if (m > 0 && n == 0) {
            return calcularAckermann(m - 1, 1);
        } else {
            return calcularAckermann(m - 1, calcularAckermann(m, n - 1));
        }
    }
}
