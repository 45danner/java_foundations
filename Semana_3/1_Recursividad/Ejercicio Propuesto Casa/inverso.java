public class inverso {
    public static void main(String[] args) {
        int n = 1000;
        imprimirEnOrdenInverso(n);
    }
    
    public static void imprimirEnOrdenInverso(int n) {
        if (n > 0) {
            System.out.println(n);
            imprimirEnOrdenInverso(n - 1);
        }
    }
}
