public class Grafico {
    public static void main(String[] args) {
        dibujarGrafico(7);
    }
    
    public static void dibujarGrafico(int n) {
        for (int i = 1; i <= n; i++) {
           
            for (int j = 1; j <= n - i; j++) {
                System.out.print(" ");
            }
            
            // Imprimir asteriscos
            for (int k = 1; k <= i; k++) {
                System.out.print(" *");
            }
            
            System.out.println();  
        }
    }
}
