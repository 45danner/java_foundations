import java.util.Random;
import java.util.Scanner;

public class Array {
    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingresa la longitud del array: ");
        int longitud = scanner.nextInt();

        int[] array = new int[longitud];

        System.out.println("Generando elementos aleatorios en el array...");

        for (int i = 0; i < longitud; i++) {
            array[i] = random.nextInt(100); 
        }

        int maximo = encontrarMaximo(array, 0);
        System.out.println("El máximo valor del array es: " + maximo);

        scanner.close();
    }

    public static int encontrarMaximo(int[] array, int index) {
        if (index == array.length - 1) {
            return array[index];
        }

        int maxResto = encontrarMaximo(array, index + 1);

        if (array[index] > maxResto) {
            return array[index];
        } else {
            return maxResto;
        }
    }
}
