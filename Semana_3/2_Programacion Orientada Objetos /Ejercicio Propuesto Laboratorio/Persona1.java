public class Persona1 {
    String nombre;
    String apellidos;
    String númeroDocumentoIdentidad;
    int añoNacimiento;
    String paísNacimiento;
    char género;

    Persona1(String nombre, String apellidos, String númeroDocumentoIdentidad, int añoNacimiento, String paísNacimiento,
            char género) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.númeroDocumentoIdentidad = númeroDocumentoIdentidad;
        this.añoNacimiento = añoNacimiento;
        this.paísNacimiento = paísNacimiento;
        this.género = género;
    }

    void imprimir() {
        System.out.println("Nombre: " + nombre);
        System.out.println("Apellidos: " + apellidos);
        System.out.println("Número de documento de identidad: " + númeroDocumentoIdentidad);
        System.out.println("Año de nacimiento: " + añoNacimiento);
        System.out.println("País de nacimiento: " + paísNacimiento);
        System.out.println("Género: " + género);
    }

    public static void main(String[] args) {
        Persona1 persona1 = new Persona1("Danner", "Perez", "17544569", 2006, "Perú", 'H');
        Persona1 persona2 = new Persona1("yaquelin", "mamani", "98576754", 1999, "Perú", 'M');

        System.out.println("-----------Persona1------------------");
        persona1.imprimir();

        System.out.println();

        System.out.println("-----------Persona2:");
        persona2.imprimir();
    }
}
