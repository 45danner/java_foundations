public class Planeta {
   String nombre = null;
   double masa = 0;
   double volumen = 0;
   int diámetro = 0;
   int distanciaSol = 0;
   int cantidadSatélites = 0;

   enum tipoPlaneta {
      GASEOSO, TERRESTRE, ENANO
   }

   boolean esObservable = false;
   tipoPlaneta tipo;
   double periodoOrbital = 0; // Nuevo atributo
   double periodoRotacion = 0; // Nuevo atributo

   Planeta(String nombre, int cantidadSatélites, double masa, double volumen, int diámetro, int distanciaSol,
         tipoPlaneta tipo, boolean esObservable, double periodoOrbital, double periodoRotacion) {
      this.nombre = nombre;
      this.cantidadSatélites = cantidadSatélites;
      this.masa = masa;
      this.volumen = volumen;
      this.diámetro = diámetro;
      this.distanciaSol = distanciaSol;
      this.tipo = tipo;
      this.esObservable = esObservable;
      this.periodoOrbital = periodoOrbital;
      this.periodoRotacion = periodoRotacion;
   }

   void imprimir() {
      System.out.println("Nombre del planeta: " + nombre);
      System.out.println("Cantidad de satélites: " + cantidadSatélites);
      System.out.println("Masa del planeta: " + masa);
      System.out.println("Volumen del planeta: " + volumen);
      System.out.println("Diámetro del planeta: " + diámetro);
      System.out.println("Distancia al sol: " + distanciaSol);
      System.out.println("Tipo de planeta: " + tipo);
      System.out.println("Es observable: " + esObservable);
      System.out.println("Periodo orbital: " + periodoOrbital);
      System.out.println("Periodo de rotación: " + periodoRotacion);
   }

   double calcularDensidad() {
      return masa / volumen;
   }

   boolean esPlanetaExterior() {
      float límite = (float) (149597870 * 3.4);
      return distanciaSol > límite;
   }

   public static void main(String args[]) {
      Planeta p1 = new Planeta("Tierra", 1, 5.9736E24, 1.08321E12, 12742, 150000000, tipoPlaneta.TERRESTRE, true, 1,24);
      p1.imprimir();
      System.out.println("Densidad del planeta: " + p1.calcularDensidad());
      System.out.println("Es planeta exterior: " + p1.esPlanetaExterior());
      System.out.println();


      Planeta p2 = new Planeta("Júpiter", 79, 1.899E27, 1.4313E15, 139820, 750000000, tipoPlaneta.GASEOSO, true, 12,10);
      p2.imprimir();
      System.out.println("Densidad del planeta: " + p2.calcularDensidad());
      System.out.println("Es planeta exterior: " + p2.esPlanetaExterior());
   }
}
