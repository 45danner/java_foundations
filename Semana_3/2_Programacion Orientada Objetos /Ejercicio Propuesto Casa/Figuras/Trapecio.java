package ejercicios;

public class Trapecio {
    double baseMayor; // Atributo que define la base mayor del trapecio
    double baseMenor; // Atributo que define la base menor del trapecio
    double altura; // Atributo que define la altura del trapecio
    double lado1; // Atributo que define uno de los lados del trapecio
    double lado2; // Atributo que define el otro lado del trapecio

    public Trapecio(double baseMayor, double baseMenor, double altura, double lado1, double lado2) {
        this.baseMayor = baseMayor;
        this.baseMenor = baseMenor;
        this.altura = altura;
        this.lado1 = lado1;
        this.lado2 = lado2;
    }

    double calcularArea() {
        return ((baseMayor + baseMenor) * altura) / 2;
    }

    double calcularPerimetro() {
        return baseMayor + baseMenor + lado1 + lado2;
    }
}
