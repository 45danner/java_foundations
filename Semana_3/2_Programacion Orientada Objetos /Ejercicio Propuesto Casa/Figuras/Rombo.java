package ejercicios;

public class Rombo {
    double diagonalMayor; // Atributo que define la diagonal mayor del rombo
    double diagonalMenor; // Atributo que define la diagonal menor del rombo

    public Rombo(double diagonalMayor, double diagonalMenor) {
        this.diagonalMayor = diagonalMayor;
        this.diagonalMenor = diagonalMenor;
    }

    double calcularArea() {
        return (diagonalMayor * diagonalMenor) / 2;
    }

    double calcularPerimetro() {
        return 4 * Math.sqrt(Math.pow(diagonalMayor / 2, 2) + Math.pow(diagonalMenor / 2, 2));
    }
}
