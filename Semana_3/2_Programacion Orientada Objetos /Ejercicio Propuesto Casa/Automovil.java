
public class Automovil {
    String marca;
    int modelo;
    int motor;
    
    enum TipoCombustible {
        GASOLINA, BIOETANOL, DIESEL, BIODISESEL, GAS_NATURAL
    }
    TipoCombustible tipoCombustible;
    
    enum TipoAutomovil {
        CIUDAD, SUBCOMPACTO, COMPACTO, FAMILIAR, EJECUTIVO, SUV
    }
    TipoAutomovil tipoAutomovil;
    
    int numeroPuertas;
    int cantidadAsientos;
    int velocidadMaxima;
    
    enum TipoColor {
        BLANCO, NEGRO, ROJO, NARANJA, AMARILLO, VERDE, AZUL, VIOLETA
    }
    TipoColor color;
    
    int velocidadActual = 0;
    boolean automatico;
    int multas = 0;
    
    Automovil(String marca, int modelo, int motor, TipoCombustible tipoCombustible, TipoAutomovil tipoAutomovil,
              int numeroPuertas, int cantidadAsientos, int velocidadMaxima, TipoColor color, boolean automatico) {
        this.marca = marca;
        this.modelo = modelo;
        this.motor = motor;
        this.tipoCombustible = tipoCombustible;
        this.tipoAutomovil = tipoAutomovil;
        this.numeroPuertas = numeroPuertas;
        this.cantidadAsientos = cantidadAsientos;
        this.velocidadMaxima = velocidadMaxima;
        this.color = color;
        this.automatico = automatico;
    }
    
    boolean isAutomatico() {
        return automatico;
    }
    
    void setAutomatico(boolean automatico) {
        this.automatico = automatico;
    }
    void acelerar(int incrementoVelocidad) {
        if (velocidadActual + incrementoVelocidad <= velocidadMaxima) {
            velocidadActual += incrementoVelocidad;
        } else {
            // Se ha sobrepasado la velocidad máxima permitida, se genera una multa
            int excesoVelocidad = velocidadActual + incrementoVelocidad - velocidadMaxima;
            multas += excesoVelocidad * 100; // Cada km/h excedido incrementa la multa en 100 unidades
            velocidadActual = velocidadMaxima;
            System.out.println("Se ha generado una multa por exceso de velocidad.");
        }
    }
    boolean tieneMultas() {
        return multas > 0;
    }

    int calcularValorMultas() {
        return multas;
    }
    
    void desacelerar(int decrementoVelocidad) {
        if (velocidadActual - decrementoVelocidad >= 0) {
            velocidadActual -= decrementoVelocidad;
        } else {
            velocidadActual = 0;
        }
    }

    void frenar() {
        velocidadActual = 0;
    }
    
    void setVelocidadActual(int velocidadActual) {
        this.velocidadActual = velocidadActual;
    }

    void imprimir() {
        System.out.println("Marca = " + marca);
        System.out.println("Modelo = " + modelo);
        System.out.println("Motor = " + motor);
        System.out.println("Tipo de combustible = " + tipoCombustible);
        System.out.println("Tipo de automóvil = " + tipoAutomovil);
        System.out.println("Número de puertas = " + numeroPuertas);
        System.out.println("Cantidad de asientos = " + cantidadAsientos);
        System.out.println("Velocida máxima = " + velocidadMaxima);
        System.out.println("Color = " + color);
        System.out.println("Velocidad actual = " + velocidadActual);
        System.out.println("Es automático = " + automatico);
        System.out.println("Multas = " + multas);
    } 
    public static void main(String args[]) {
    Automovil auto1 = new Automovil("Ford", 2018, 3, TipoCombustible.DIESEL, TipoAutomovil.EJECUTIVO, 5, 6, 50, TipoColor.NEGRO, true);
    auto1.imprimir();
    auto1.setVelocidadActual(100);
    System.out.println("Velocidad actual = " + auto1.velocidadActual);
    auto1.acelerar(200);
    System.out.println("Velocidad actual = " + auto1.velocidadActual);
    System.out.println("Multas = " + auto1.calcularValorMultas());
    auto1.desacelerar(50);
    System.out.println("Velocidad actual = " + auto1.velocidadActual);
    auto1.frenar();
    System.out.println("Velocidad actual = " + auto1.velocidadActual);
    auto1.desacelerar(20);


    }
}  
