import java.math.BigInteger;
public class EJE_15_CS {
    public static void main(String[] args) {
        BigInteger bigInt1 = new BigInteger("623");
        BigInteger bigInt2 = new BigInteger("987");

        BigInteger sum = bigInt1.add(bigInt2);
        BigInteger res = bigInt1.subtract(bigInt2);
        BigInteger mult = bigInt1.multiply(bigInt2);
        BigInteger div = bigInt1.divide(bigInt2);

      
        System.out.println("Suma: " + sum);
        System.out.println("Resta: " + res);
        System.out.println("Multiplicación: " + mult);
        System.out.println("División: " + div);
    }
}
