import java.math.BigDecimal;
public class EJE_14_CS {
    public static void main(String[] args) {
        BigDecimal num1 = new BigDecimal("10.5");
        BigDecimal num2 = new BigDecimal("3");
    
        
        BigDecimal sum = num1.add(num2);
        BigDecimal res= num1.subtract(num2);
        BigDecimal mult = num1.multiply(num2);
        BigDecimal div = num1.divide(num2);
    
        System.out.println("Suma: " + sum);
        System.out.println("Resta: " + res);
        System.out.println("Multiplicación: " + mult);
        System.out.println("División: " + div);
      }
}
