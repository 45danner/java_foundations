import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.DateFormat;
public class EJE_12_CS {
    public static void main(String[] args) {
        Date fechaHoraActual = new Date();
        DateFormat formatoFechaHora = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String fechaHoraFormateada = formatoFechaHora.format(fechaHoraActual);
        System.out.println("La fecha y hora actual es: " + fechaHoraFormateada);
     }
}
