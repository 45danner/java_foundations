import java.text.SimpleDateFormat;
import java.util.Date;
public class EJE_13_CS {
    public static void main(String[] args) {
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        Date fecha = new Date();
        String fechaFormateada = formato.format(fecha);
        System.out.println("Fecha formateada: " + fechaFormateada);
    }
}
