import java.util.Scanner;
import java.lang.Math;
public class EJE_9_CS {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el cuadrado: ");
        double lado = sc.nextDouble();
        
        double area = Math.pow(lado, 2);
        
        System.out.println(area);
    }
}
