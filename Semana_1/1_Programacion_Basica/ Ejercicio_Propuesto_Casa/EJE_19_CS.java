import java.io.FileOutputStream;
import java.io.OutputStream;
public class EJE_19_CS {
    public static void main(String[] args) {
        try {
            OutputStream os = new FileOutputStream("archivo.txt");
            String texto = "Hola, senati!";
            os.write(texto.getBytes());
            os.close();
            System.out.println("Se escribió en el archivo.");
        } catch (Exception e) {
            System.out.println("Ocurrió un error al escribir en el archivo.");
            e.printStackTrace();
        }
    }
}
