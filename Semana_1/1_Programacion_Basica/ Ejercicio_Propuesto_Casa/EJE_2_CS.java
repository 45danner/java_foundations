import java.util.HashMap;
public class EJE_2_CS {
    public static void main(String[] args) {
        HashMap<String, Integer> personas = new HashMap<>();

        personas.put("Juan", 30);
        personas.put("María", 25);
        personas.put("Pedro", 40);

        System.out.println(personas);
    }
}
