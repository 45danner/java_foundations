import java.util.regex.Pattern;
public class EJE_10_CS {
    
    public static void main(String[] args) {
        String regex = "[a-z]+[0-9]+";
        
        Pattern pattern = Pattern.compile(regex);
        
        String cadena = "ab3xygh";
        boolean encontrado = pattern.matcher(cadena).matches();
        System.out.println("Encontrado " + encontrado);
    }
}
