import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
public class EJE_7_CS {
    public static void main(String[] args) {
        String rutaArchivo = "ruta/al/archivo.txt";
        BufferedReader lector = null;
        
        try {
            lector = new BufferedReader(new FileReader(rutaArchivo));
            String linea;
            
            while ((linea = lector.readLine()) != null) {
                System.out.println(linea);
            }
        } catch (IOException e) {
            System.out.println("Error al leer el archivo: " + e.getMessage());
        } finally {
            try {
                if (lector != null) {
                    lector.close();
                }
            } catch (IOException e) {
                System.out.println("Error al cerrar el lector: " + e.getMessage());
            }
        }
    }
}
