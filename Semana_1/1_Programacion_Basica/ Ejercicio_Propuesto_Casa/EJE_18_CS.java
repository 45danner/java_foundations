import java.io.FileInputStream;
import java.io.IOException;

public class EJE_18_CS {
    public static void main(String[] args) {
        try (FileInputStream fis = new FileInputStream("ruta/archivo.txt")) {
            int byteLeido;
            while ((byteLeido = fis.read()) != -1) {
                System.out.print((char) byteLeido);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
