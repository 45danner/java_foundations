import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class EJE_11_CS {
    public static void main(String[] args) {
        String regex = "patrón";
        String texto = "cadena con patrón";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(texto);
        
        if (matcher.find()) {
            System.out.println("Se encontró el patrón en la cadena.");
        } else {
            System.out.println("No se encontró el patrón en la cadena.");
        }
    }
}
