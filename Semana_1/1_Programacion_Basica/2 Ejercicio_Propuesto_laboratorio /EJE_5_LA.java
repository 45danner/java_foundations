package Ejercicio_Propuesto_Labaratorio;
import java.util.Scanner;
public class EJE_5_LA {
    public static void main(String[] args) {
        Scanner ingreso = new Scanner(System.in);
        System.out.print("Ingrese el primer número: ");
        int num1 = ingreso.nextInt();
        System.out.print("Ingrese el segundo número: ");
        int num2 = ingreso.nextInt();
        int diferencia = num1 - num2;
        System.out.println("La diferencia entre " + num1 + " y " + num2 + " es " + diferencia);
    }
}
