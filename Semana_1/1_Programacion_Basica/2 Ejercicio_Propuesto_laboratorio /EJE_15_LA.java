package Ejercicio_Propuesto_Labaratorio;
import java.util.Scanner;
public class EJE_15_LA {
    public static void main(String[] args) {
        Scanner ingreso = new Scanner(System.in);
        int num;
        boolean esPrimo = true;

        System.out.print("Ingrese un numero : ");
        num = ingreso.nextInt();

        for (int i = 2; i <= num / 2; i++) {
            if (num % i == 0) {
                esPrimo = false;
                break;
            }
        }

        if (esPrimo) {
            System.out.println(num + " es un numero primo.");
        } else {
            System.out.println(num + " no es un numero primo.");
        }
    }
}
