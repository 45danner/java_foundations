package Ejercicio_Propuesto_Labaratorio;
import java.util.Scanner;

public class EJE_6_LA {
    public static void main(String[] args) {
        Scanner ingreso= new Scanner(System.in);
        System.out.print("Ingrese el primer número: ");
        int num1 = ingreso.nextInt();
        System.out.print("Ingrese el segundo número: ");
        int num2 = ingreso.nextInt();
        
        int cociente = num1 / num2;
        int resto = num1 % num2;
        
        System.out.println("El cociente de " + num1 + " entre " + num2 + " es " + cociente);
        System.out.println("El resto de " + num1 + " entre " + num2 + " es " + resto);
    }
}
