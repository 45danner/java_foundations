package Ejercicio_Propuesto_Labaratorio;
import java.util.Scanner;
public class EJE_10_LA {
    public static void main(String[] args) {
        Scanner ingreso = new Scanner(System.in);
        System.out.print("Ingrese un número entero: ");
        int num = ingreso.nextInt();

        if (num % 2 == 0) {
            System.out.println(num + " es par");
        } else {
            System.out.println(num + " es impar");
        }
    }
}
