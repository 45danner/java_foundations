package Ejercicio_Propuesto_Labaratorio;
import java.util.Scanner;

public class EJE_18_LA {
    public static void main(String[] args) {
        Scanner ingreso = new Scanner(System.in);
        System.out.print("Ingrese un número entero: ");
        int num = ingreso.nextInt();
        
        int factorial = 1;
        for (int i = 2; i <= num; i++) {
            factorial *= i;
        }
        
        System.out.println("El factorial es" + factorial);
    }
}
