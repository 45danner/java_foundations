package Ejercicio_Propuesto_Labaratorio;
import java.util.Scanner;
public class EJE_7_LA {
    public static void main(String[] args) {
        Scanner ingreso = new Scanner(System.in);
        System.out.print("Ingrese la base del triángulo: ");
        double base = ingreso.nextDouble();
        System.out.print("Ingrese la altura del triángulo: ");
        double altura = ingreso.nextDouble();
        
        double area = (base * altura) / 2;
        
        System.out.println("El área del triángulo es: " + area);
    }
}
