package Ejercicio_Propuesto_Labaratorio;
import java.util.Scanner;

public class EJE_16_LA {
    public static void main(String[] args) {
        Scanner ingreso = new Scanner(System.in);
        System.out.print("Ingrese su peso en kilogramos: ");
        double peso = ingreso.nextDouble();
        System.out.print("Ingrese su altura en metros: ");
        double altura = ingreso.nextDouble();
        
        double imc = peso / (altura * altura);
        
        System.out.println("Su IMC es " + imc);
    }
}
