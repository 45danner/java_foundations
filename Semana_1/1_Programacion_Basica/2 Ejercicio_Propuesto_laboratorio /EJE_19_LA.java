package Ejercicio_Propuesto_Labaratorio;
import java.util.Scanner;

public class EJE_19_LA {
    public static void main(String[] args) {
        Scanner ingreso = new Scanner(System.in);
        System.out.print("Ingrese un número entero: ");
        int num = ingreso.nextInt();
        
        int suma = 0;
        for (int i = 1; i <= num; i++) {
            suma += i;
        }
        
        System.out.println("La suma de todos los números enteros desde 1 hasta " + num + " es " + suma);
    }
}
