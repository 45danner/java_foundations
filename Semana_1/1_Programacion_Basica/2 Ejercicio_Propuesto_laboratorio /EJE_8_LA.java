package Ejercicio_Propuesto_Labaratorio;
import java.util.Scanner;
public class EJE_8_LA {
    public static void main(String[] args) {
        Scanner ingreso = new Scanner(System.in);

        System.out.print("Ingrese la longitud del lado 1: ");
        double lado1 = ingreso.nextDouble();

        System.out.print("Ingrese la longitud del lado 2: ");
        double lado2 = ingreso.nextDouble();

        System.out.print("Ingrese la longitud del lado 3: ");
        double lado3 = ingreso.nextDouble();

        if (lado1 == lado2 && lado1 == lado3) {
            System.out.println("El triángulo es equilátero.");
        } else if (lado1 == lado2 || lado1 == lado3 || lado2 == lado3) {
            System.out.println("El triángulo es isósceles.");
        } else {
            System.out.println("El triángulo es escaleno.");
        }
    }
}
