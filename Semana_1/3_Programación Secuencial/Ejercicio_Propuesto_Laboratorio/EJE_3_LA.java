/*Realizar un programa que calcule el area y perímetro de una elipse. */
import java.util.Scanner;
public class EJE_3_LA {
    public static void main(String[] args) {
        Scanner ingre = new Scanner(System.in);

        System.out.print("Ingrese el valor del semi eje a: ");
        double semieje_a = ingre.nextDouble();

        System.out.print("Ingrese el valor del semi eje b: ");
        double semieje_b = ingre.nextDouble();

        double area = Math.PI * semieje_a * semieje_b;
        double perimetro = 2 * Math.PI * Math.sqrt((semieje_a * semieje_a + semieje_b * semieje_b) / 2);

        System.out.println("El área de la elipse es: " + area);
        System.out.println("El perímetro de la elipse es: " + perimetro);
    }
}

/*____Área = π * semieje_a * semieje_b
______Perímetro = 2 * π * sqrt((semieje_a^2 + semieje_b^2) / 2)*/