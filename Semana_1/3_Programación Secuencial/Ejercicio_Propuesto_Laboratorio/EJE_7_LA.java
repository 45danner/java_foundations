/* Una tienda ofrece un descuento del 15% sobre el total 
de la compra y un cliente desea saber 
cuánto deberá pagar finalmente por su compra.*/
import java.util.Scanner;
public class EJE_7_LA {
    public static void main(String[] args) {
        System.out.println("-----DESCUENTO---");
        Scanner ingre = new Scanner(System.in);
        System.out.print("ingrese el precio:");
        double preO = ingre.nextDouble();
        double preF = preO - preO * 0.15;
        System.out.println("Su descuento es de :"+preF);
    }
}
