/* Escriba un programa que realice la conversión de grados Celsius (°C) a grados Fahrenheit (°F). La ecuación de conversión es:
F=95∗C+32F = 95*C + 32F=95∗C+32*/
import java.util.Scanner;
public class EJE_5_LA {
    public static void main(String[] args) {
        
        Scanner ing = new Scanner(System.in);

        System.out.print("Ingrese en grados Celsius: ");
        double celsius = ing.nextDouble();

        double fahrenheit = 9.0 / 5.0 * celsius + 32;

        System.out.print(celsius + " grados Celsius equivalen a " + fahrenheit + " grados Fahrenheit.");
    }
}
