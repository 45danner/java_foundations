/* En una playa de estacionamiento cobran S/. 2.5 por hora o
 fracción. Diseñe un algoritmo que determine cuanto debe
pagar un cliente por el estacionamiento de su vehículo, 
conociendo el tiempo de estacionamiento en horas y minutos.
*/
import java.util.Scanner;
public class EJE_8_LA {
    public static void main(String[] args) {
        
        Scanner ingre = new Scanner(System.in);
        System.out.print("Cuantas horas a  estacionado: ");
        int horas = ingre.nextInt();
        System.out.print("Cuantos minitos a estacionado:");
        int minutos = ingre.nextInt();
        double costo = 0;

        if (minutos > 0) {
            horas++;
        }
        costo = horas * 2.5;
        System.out.println("El costo del estacionamiento es S/." + costo);
        
    }
}
