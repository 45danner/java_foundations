/* Calcular el área de un trapecio a partir de sus cuatro lados.*/
import java.util.Scanner;
public class EJE_2_LA {
    public static void main(String[] args) {
        System.out.println("--calculando el area de un trapecio--");
        Scanner ingreso = new Scanner(System.in);
        System.out.print("Ingrese base mayor ");
        double a = ingreso.nextDouble();
        System.out.print(" Ingrese base menor");
        double b = ingreso.nextDouble();
        System.out.print("Ingrese la altura ");
        double h = ingreso.nextDouble();
        double area = ((a + b) * h) /2 ;
        System.out.print(" " + area);
    }
}
