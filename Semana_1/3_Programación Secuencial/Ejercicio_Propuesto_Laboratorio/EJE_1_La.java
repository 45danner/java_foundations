/* Calcular la raíz cuadrada de un número entero.*/

import java.util.Scanner;


public class EJE_1_La{
    public static void main(String[] args) {
        Scanner ingreso = new Scanner(System.in);
        System.out.print("Ingrese un numero humano:");
        double num = ingreso.nextDouble();
        double raiz_Cuadrada = Math.sqrt(num);
        System.out.println("La raiz cuadrada  es " + raiz_Cuadrada);
      }

}
