import java.util.Scanner;

public class EJE_CS_9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double lado1, lado2, lado3;
        double s, area;

        System.out.println("Introduce el primera lado :");
        lado1 = sc.nextDouble();
        System.out.println("Introduce el segundo lado :");
        lado2 = sc.nextDouble();
        System.out.println("Introduce el tercer lado :");
        lado3 = sc.nextDouble();
        s = (lado1 + lado2 + lado3) / 2;
        area = Math.sqrt(s * s - lado1 * s - lado2 * s - lado3);

        System.out.println("El área del triángulo es: " + area);
    }
}
