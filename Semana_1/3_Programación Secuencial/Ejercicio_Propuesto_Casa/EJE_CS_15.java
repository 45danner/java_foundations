import java.util.Scanner;

public class EJE_CS_15 {
    public static void main(String[] args) {
        Scanner scR = new Scanner(System.in);

        System.out.print("Ingresa un nUmero entero: ");
        int n = scR.nextInt();
        System.out.print("Ingresa la cantidad de cifras a quitar: ");
        int m = scR.nextInt();

        int potencia = (int) Math.pow(10, m);
        int resultado = n / potencia;

        System.out.println("El resultado es: " + resultado);
    }
}
