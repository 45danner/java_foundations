import java.util.Scanner;

public class EJE_CS_12 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.print("Introduce un número de 5 cifras: ");
    int num = sc.nextInt();
    System.out.println("Las cifras del número " + num + " desde el final son: ");
    for(int i = 4; i >= 0; i--) {
      System.out.println(num % 10);
      num /= 10;
    }
  }
}
