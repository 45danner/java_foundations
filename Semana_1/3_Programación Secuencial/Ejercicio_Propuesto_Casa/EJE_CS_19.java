import java.util.Scanner;

public class EJE_CS_19 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = 0;
        System.out.print("Ingrese un numero : ");
        int number = scanner.nextInt();
        while (number != 0) {
            if (number % 10 == 2) {
                count++;
            }
            System.out.print("Ingrese un numero : ");
            number = scanner.nextInt();
        }
        System.out.println("Se ingresaron " + count + " números que acaban en 2.");
        scanner.close();
    }
}
