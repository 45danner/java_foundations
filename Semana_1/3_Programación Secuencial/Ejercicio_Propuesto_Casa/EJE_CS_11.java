import java.util.Scanner;

public class EJE_CS_11 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduce un número entero de 5 cifras: ");
        int numero = sc.nextInt();
        sc.close();

        System.out.println("Cifras del número desde el principio:");
        int divisor = 10000;
        for (int i = 1; i <= 5; i++) {
            int cifra = numero / divisor;
            System.out.println(cifra);
            numero = numero % divisor;
            divisor = divisor / 10;
        }
    }
}
