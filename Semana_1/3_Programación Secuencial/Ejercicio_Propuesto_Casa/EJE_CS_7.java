import java.util.Scanner;

public class EJE_CS_7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double cateto1, cateto2, hipotenusa;
        System.out.println("Ingrese la longitud del primer cateto: ");
        cateto1 = sc.nextDouble();
        System.out.println("Ingrese la longitud del segundo cateto: ");
        cateto2 = sc.nextDouble();
        hipotenusa = Math.sqrt(Math.pow(cateto1, 2) + Math.pow(cateto2, 2));

        System.out.println("La longitud de la hipotenusa es: " + hipotenusa);
    }
}
