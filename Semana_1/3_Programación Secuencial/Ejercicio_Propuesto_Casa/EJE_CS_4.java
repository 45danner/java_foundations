import java.util.Scanner;
public class EJE_CS_4 {
    public static void main(String[] args) {
        
        Scanner ing = new Scanner(System.in);

        System.out.print("Ingrese en grados Celsius: ");
        double centigr = ing.nextDouble();

        double fahrenheit = 32 + 9* centigr/5;

        System.out.print(centigr + " citigrados equivalen a " + fahrenheit + " grados Fahrenheit.");
    }
}
