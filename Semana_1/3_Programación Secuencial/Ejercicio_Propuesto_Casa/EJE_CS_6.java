import java.util.Scanner;

public class EJE_CS_6 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduce la velocidad en km/h: ");
        double velo = sc.nextDouble();
        double velom = velo / 3.6;
        System.out.println("La velocidad en m/s es: " + velom);
    }
}
