import java.util.Scanner;

public class EJE_CS_14 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.print("Ingrese el precio : ");
        double precio = input.nextDouble();
        
        System.out.print("Ingrese el descuento : ");
        double descuento = input.nextDouble();
        
        double precioFinal = precio - precio * descuento;
        
        System.out.println("El precio final del producto es: $" + precioFinal);
        
        input.close();
    }
}
//DNR-DENY-DANY-