import java.util.Scanner;

public class EJE_CS_17 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String respuesta = "S";
        while (respuesta.equals("S")) {
            System.out.print("Introduce los grados centígrados: ");
            double celsius = sc.nextDouble();
            double kelvin = celsius + 273.15;
            System.out.println("Los grados kelvin son: " + kelvin);
            System.out.print("Repetir proceso (S/N): ");
            respuesta = sc.next();
        }
        System.out.println("---Fin del programa---");
    }
}
