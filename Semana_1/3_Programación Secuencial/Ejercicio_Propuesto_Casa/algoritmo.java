/*import java.io.File;

public class algoritmo {
    public static void main(String[] args) {
        String nombreArchivo = "EJE_1_CS"; // Nombre base del archivo
        int numArchivos = 20; // Cantidad de archivos a eliminar
        
        for (int i = 1; i <= numArchivos; i++) {
            File archivo = new File(nombreArchivo + i + ".java");
            if (archivo.delete()) {
                System.out.println("El archivo " + archivo.getName() + " ha sido eliminado.");
            } else {
                System.out.println("El archivo " + archivo.getName() + " no ha podido ser eliminado.");
            }
        }
    }
}
*/

import java.io.FileWriter;
import java.io.IOException;

public class algoritmo {
    public static void main(String[] args) {
        String nombreArchivo = "EJE_CS_";
        String extension = ".java";
        int cantidadArchivos = 20;

        for (int i = 1; i <= cantidadArchivos; i++) {
            String nombreCompleto = nombreArchivo + i + extension;
            try {
                FileWriter archivo = new FileWriter(nombreCompleto);
                archivo.close();
                System.out.println("Se ha creado el archivo " + nombreCompleto);
            } catch (IOException e) {
                System.out.println("Error al crear el archivo " + nombreCompleto);
                e.printStackTrace();
            }
        }
    }
}