import java.util.Scanner;

public class EJE_CS_5 {
    public static void main(String[] args) {
        double radio, longitud, area;
        final double PI = 3.14159265359;
        Scanner sc = new Scanner(System.in);

        System.out.print("Ingrese el radio de la circunferencia: ");
        radio = sc.nextDouble();

        longitud = 2 * PI * radio;
        area = PI * Math.pow(radio, 2);

        System.out.println("La longitud de la circunferencia es: " + longitud);
        System.out.println("El area de la circunferencia es: " + area);
    }
}
