import java.util.Scanner;

public class EJE_CS_10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numero, cifra1, cifra2, cifra3;

        System.out.print("Introduce un numero de tres cifras: ");
        numero = sc.nextInt();

        cifra1 = numero / 100;
        cifra2 = (numero / 10) % 10;
        cifra3 = numero % 10;

        System.out.println("La primera cifra es: " + cifra1);
        System.out.println("La segunda cifra es: " + cifra2);
        System.out.println("La tercera cifra es: " + cifra3);

        sc.close();
    }
}
