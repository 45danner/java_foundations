import java.util.Scanner;

public class EJE_CS_16 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        
        System.out.print("Introduzca la temperatura en grados Celsius: ");
        double celsius = in.nextDouble();
    
        double kelvin = celsius + 273.15;
        System.out.println("La temperatura en grados Kelvin es: " + kelvin);
    
        double reamur = celsius * 0.8;
        System.out.println("La temperatura en grados Reamur es: " + reamur);
    }
}
