import java.util.Scanner;

public class EJE_CS_3 {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.print("Introduce un número entero: ");
        int num = in.nextInt();

        int doble = num * 2;
        int triple = num * 3;

        System.out.println("El doble de " + num + " es " + doble);
        System.out.println("El triple de " + num + " es " + triple);

    }
}
