import java.util.Scanner;

public class EJE_4_LA {
    {
        Scanner input = new Scanner(System.in);
        System.out.print("Ingrese el primer lado del triángulo: ");
        int lado1 = input.nextInt();
        System.out.print("Ingrese el segundo lado del triángulo: ");
        int lado2 = input.nextInt();
        System.out.print("Ingrese el tercer lado del triángulo: ");
        int lado3 = input.nextInt();

        if (lado1 + lado2 > lado3 && lado1 + lado3 > lado2 && lado2 + lado3 > lado1) {
            System.out.println("Los lados ingresados pueden formar un triángulo válido.");
        } else {
            System.out.println("Los lados ingresados no pueden formar un triángulo válido.");
        }
    }
}
