import java.util.Scanner;

public class EJE_13_LA {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese la cantidad de números a evaluar: ");
        int n = sc.nextInt();
        int count = 0;
        for (int i = 0; i < n; i++) {
            System.out.print("Ingrese un número entero: ");
            int num = sc.nextInt();
            if (isFibonacci(num)) {
                System.out.println(num + " es un número de Fibonacci.");
                count++;
            } else {
                System.out.println(num + " no es un número de Fibonacci.");
            }
        }
        System.out.println("Hay " + count + " números de Fibonacci en la lista.");
    }

    public static boolean isPerfectSquare(int x) {
        int s = (int) Math.sqrt(x);
        return (s * s == x);
    }

    public static boolean isFibonacci(int n) {
        return isPerfectSquare(5 * n * n + 4) || isPerfectSquare(5 * n * n - 4) || n == 0 || n == 1;
    }
    
    
}
