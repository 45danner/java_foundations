import java.util.ArrayList;
import java.util.Scanner;

public class EJE_14_LA {
    
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        ArrayList<String> palabras = new ArrayList<String>();

        System.out.println("Ingrese una lista de palabras separadas por espacios:");
        String[] palabrasInput = input.nextLine().split(" ");

        for (String palabra : palabrasInput) {
            palabras.add(palabra);
        }

        int contadorPalindromos = 0;
        for (String palabra : palabras) {
            if (esPalindromo(palabra)) {
                contadorPalindromos++;
            }
        }

        System.out.println("Hay " + contadorPalindromos + " palíndromos en la lista.");
    }

    public static boolean esPalindromo(String palabra) {
        String reverso = new StringBuilder(palabra).reverse().toString();
        return palabra.equals(reverso);
    }
}
