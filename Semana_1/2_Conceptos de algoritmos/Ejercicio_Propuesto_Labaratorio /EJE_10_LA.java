import java.util.Scanner;
public class EJE_10_LA {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese la cantidad de números: ");
        int cantidad = scanner.nextInt();
        
        int contadorPares = 0;
        for (int i = 0; i < cantidad; i++) {
            System.out.print("Ingrese un número entero: ");
            int numero = scanner.nextInt();
            
            if (numero % 2 == 0) {
                contadorPares++;
            }
        }
        
        System.out.println("La cantidad de números pares es: " + contadorPares);
    }
    
    
}
