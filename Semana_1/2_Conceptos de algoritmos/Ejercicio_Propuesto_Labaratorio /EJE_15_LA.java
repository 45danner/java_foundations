import java.util.Scanner;

public class EJE_15_LA {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = 0;
        System.out.print("Ingrese las palabras separadas por comas: ");
        String input = scanner.nextLine();
        String[] words = input.split(",");
        for (String word : words) {
            if (word.toLowerCase().startsWith("a")) {
                count++;
            }
        }
        System.out.println("Hay " + count + " palabras que comienzan con la letra 'a'.");
    }
}
