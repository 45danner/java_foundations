import java.util.Scanner;
public class EJE_20_LA {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el número de divisores: ");
        int numDivisores = sc.nextInt();
        System.out.print("Ingrese la cantidad de números enteros: ");
        int n = sc.nextInt();
        int count = 0;
        for (int i = 0; i < n; i++) {
            System.out.print("Ingrese el número " + (i+1) + ": ");
            int num = sc.nextInt();
            int divisors = 0;
            for (int j = 1; j <= num; j++) {
                if (num % j == 0) {
                    divisors++;
                }
            }
            if (divisors > numDivisores) {
                count++;
            }
        }
        System.out.println("La cantidad de números enteros con más de " + numDivisores + " divisores es: " + count);
        sc.close();
    }
}
