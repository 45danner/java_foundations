import java.util.Scanner;;
public class EJE_2_LA {
    public static void main(String[] args) {
        Scanner ingreso = new Scanner(System.in);
        System.out.print("Ingrese un número entero: ");
        int num = ingreso.nextInt();

        if (num > 0) {
            System.out.println(num + " es un número positivo.");
        } else if (num < 0) {
            System.out.println(num + " es un número negativo.");
        } else {
            System.out.println("El número es cero.");
        }
    }
}
