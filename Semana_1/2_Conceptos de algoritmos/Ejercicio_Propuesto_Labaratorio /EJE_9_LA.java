import java.util.Scanner;
public class EJE_9_LA {
    public static void main(String[] args) {
        Scanner ingreso = new Scanner(System.in);
        int num;
        boolean es_Primo = true;

        System.out.print("Ingrese un numero : ");
        num = ingreso.nextInt();

        for (int i = 2; i <= num / 2; i++) {
            if (num % i == 0) {
                es_Primo = false;
                break;
            }
        }

        if (es_Primo) {
            System.out.println(num + " es un numero primo.");
        } else {
            System.out.println(num + " no es un numero primo.");
        }
    }
}
