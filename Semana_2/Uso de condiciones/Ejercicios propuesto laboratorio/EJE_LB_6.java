/*Escriba un programa que dibuje un cuadrado donde los lados se pintarán de la siguiente forma:

Por ejemplo si el lado es 7, se debe dibujar de la siguiente forma:

* * * * * * *
* * * * * * * 
* * * * * * *
* * * * * * *
* * * * * * *
* * * * * * *
* * * * * * **/
import java.util.Scanner;

public class EJE_LB_6 {
    public static void main(String[] args) {
        Scanner sn =new Scanner(System.in);
        System.out.print("ingrese: ");
        int ld = sn.nextInt();
        
        for (int i = 0; i < ld; i++) {
            for (int j = 0; j < ld; j++) {
                System.out.print(" * ");
            }
            System.out.println();
        }
    }
}