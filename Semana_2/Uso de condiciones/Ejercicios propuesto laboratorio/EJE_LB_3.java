/*Mostrar los números primos desde 2 hasta "N" */
import java.util.Scanner;;
public class EJE_LB_3 {
    public static void main(String[] args) {
        Scanner sn = new Scanner(System.in);
        System.out.print("ingrese un numero: ");
        int num=sn.nextInt();

        int i, j;
        boolean esPrimo;
        for (i = 2; i <= num ; i++) {
            esPrimo = true;
            for (j = 2; j < i; j++) {
                if (i % j == 0) {
                    esPrimo = false;
                    break;
                }
            }
            if (esPrimo) {
                System.out.print(i + " ");
            }
        }
    }
}