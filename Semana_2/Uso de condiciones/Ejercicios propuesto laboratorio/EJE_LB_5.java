/*Escribir un programa que permita calcular el Mínimo Común Multiplo de "N" numeros enpleando el bucle for.
 */
import java.util.Scanner;
public class EJE_LB_5 {
    public static void main(String[] args) {
        Scanner sn = new Scanner(System.in);
        System.out.print("ingre el primer numero:");
        int num1 = sn.nextInt();
        System.out.print("Ingrese en segundo numero:");
        int num2 = sn.nextInt();
 
        int res = mcm(num1, num2);
        System.out.println("MCM: " + res);
 
    }
    public static int mcm(int num1, int num2) {
        int a = Math.max(num1, num2);
        int b = Math.min(num1, num2);
 
        int resultado = (a / mcd(num1, num2)) * b;
         
        return resultado;
    }
    public static int mcd(int num1, int num2) {
 
        int a = Math.max(num1, num2);
        int b = Math.min(num1, num2);
 
        int resultado = 0;
        do {
            resultado = b;
            b = a % b;
            a = resultado;
 
        } while (b != 0);
        return resultado;
    }
}