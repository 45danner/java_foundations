import java.util.Scanner;
public class EJE_CS_16 {
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese un número entero: ");
        int n = sc.nextInt();
        sc.close();
        
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print((2*j-1) + " ");
            }
            System.out.println();
        }
    }
}
