import java.util.Scanner;

public class EJE_CS_17 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el tamaño del vector: ");
        int n = sc.nextInt();
        int[] vector = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.print("Ingrese el elemento " + (i+1) + ": ");
            vector[i] = sc.nextInt();
        }
        int maximo = vector[0];
        int minimo = vector[0];
        for (int i = 1; i < n; i++) {
            if (vector[i] > maximo) {
                maximo = vector[i];
            }
            if (vector[i] < minimo) {
                minimo = vector[i];
            }
        }
        System.out.println("El máximo valor del vector es: " + maximo);
        System.out.println("El mínimo valor del vector es: " + minimo);
        
        sc.close();
    }
}
