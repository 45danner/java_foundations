import java.util.Scanner;

public class EJE_CS_20 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n, count = 0, num = 1, sum = 0;
        System.out.print("Ingrese el valor de N: ");
        n = sc.nextInt();

        while (count < n) {
            int divSum = 0;
            for (int i = 1; i < num; i++) {
                if (num % i == 0) {
                    divSum += i;
                }
            }
            if (divSum == num) {
                sum += num;
                count++;
            }
            num++;
        }

        System.out.println("La suma de los primeros " + n + " números perfectos es: " + sum);
    }
}
