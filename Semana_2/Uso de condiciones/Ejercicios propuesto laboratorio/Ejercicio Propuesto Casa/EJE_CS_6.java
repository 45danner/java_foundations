import java.util.Scanner;

public class EJE_CS_6 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el tamaño de la matriz: ");
        int n = sc.nextInt();
        int[] numeros = new int[n];
        System.out.println("Ingrese los números:");
        for (int i = 0; i < n; i++) {
            numeros[i] = sc.nextInt();
        }
        for (int i = 1; i < n; i++) {
            int valorActual = numeros[i];
            int j = i - 1;
            while (j >= 0 && numeros[j] > valorActual) {
                numeros[j + 1] = numeros[j];
                j--;
            }
            numeros[j + 1] = valorActual;
        }
        System.out.println("La matriz ordenada es:");
        for (int i = 0; i < n; i++) {
            System.out.print(numeros[i] + " ");
        }
        sc.close();
    }
}
