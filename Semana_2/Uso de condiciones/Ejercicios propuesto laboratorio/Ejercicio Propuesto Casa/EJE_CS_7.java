import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class EJE_CS_7 {
    public static void main(String[] args) {
        Scanner sn = new Scanner(System.in);
        System.out.print("Ingrese el valor de a: ");
        int a = sn.nextInt();
        System.out.print("Ingrese el valor de b: ");
        int b = sn.nextInt();
        System.out.print("Ingrese la cantidad de términos a imprimir: ");
        int n = sn.nextInt();

        List<Integer> sucesion = new ArrayList<>();
        sucesion.add(a);
        sucesion.add(b);
        for (int i = 3; i <= n; i++) {
            int siguiente = siguienteUlam(sucesion, i);
            sucesion.add(siguiente);
        }
        System.out.println("Los primeros " + n + " términos de la sucesión de Ulam son:");
        for (int i = 0; i < n; i++) {
            System.out.print(sucesion.get(i) + " ");
        }
    }
    public static int siguienteUlam(List<Integer> sucesion, int n) {
        int menorSuma = Integer.MAX_VALUE;
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n - 1; j++) {
                int suma = sucesion.get(i) + sucesion.get(j);
                if (suma > sucesion.get(n - 2) && suma < menorSuma && !sucesion.contains(suma)) {
                    menorSuma = suma;
                }
            }
        }
        return menorSuma;
    }
}
