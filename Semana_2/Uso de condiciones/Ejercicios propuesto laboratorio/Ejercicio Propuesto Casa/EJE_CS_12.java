import java.util.Scanner;

public class EJE_CS_12 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Ingresa la cantidad de números primos que deseas mostrar: ");
        int n = sc.nextInt();

        int i = 2;
        int count = 0;

        while (count < n) {
            boolean esPrimo = true;
            for (int j = 2; j <= i / 2; j++) {
                if (i % j == 0) {
                    esPrimo = false;
                    break;
                }
            }
            if (esPrimo) {
                System.out.print(i + " ");
                count++;
            }
            i++;
        }
    }
}
