import java.util.Scanner;

public class EJE_CS_19 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese un numero primo: ");
        int N = sc.nextInt();

        boolean[] esPrimo = new boolean[N+1];
        for (int i = 2; i <= N; i++) {
            esPrimo[i] = true;
        }
        esPrimo[0] = false;
        esPrimo[1] = false;

        for (int i = 2; i*i <= N; i++) {
            if (esPrimo[i]) {
                for (int j = i*i; j <= N; j += i) {
                    esPrimo[j] = false;
                }
            }
        }

        System.out.println("Los primeros " + N + " numeros primos son:");
        int count = 0;
        for (int i = 2; i <= N; i++) {
            if (esPrimo[i]) {
                System.out.print(i + " ");
                count++;
            }
            if (count == 10) {
                System.out.println();
                count = 0;
            }
        }
    }
}
