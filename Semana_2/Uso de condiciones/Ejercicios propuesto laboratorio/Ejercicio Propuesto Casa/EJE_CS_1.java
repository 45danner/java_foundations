import java.util.Scanner;

public class EJE_CS_1 {
    public static void main(String[] args) {
        Scanner sn = new Scanner(System.in);
        System.out.print("Ingresa una palabra: ");
        String tex = sn.nextLine();
        int i = 0;
        while (i < tex.length()) {
            System.out.println("Letra " + (i + 1) + ": " + tex.substring(i, i + 1));
            i++;
        }
    }
} 