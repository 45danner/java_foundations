import java.util.Scanner;

public class EJE_CS_9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingrese el primer término (a): ");
        double a = input.nextDouble();

        System.out.print("Ingrese la razón común (r): ");
        double r = input.nextDouble();

        System.out.print("Ingrese el número de términos (n): ");
        int n = input.nextInt();

        double sum = a * (1 - Math.pow(r, n)) / (1 - r);
        System.out.println("La suma de los primeros " + n + " términos de la serie geométrica es: " + sum);

        input.close();
    }
}
