public class EJE_CS_15 {
    public static void main(String[] args) {
        double saldoPendiente = 5000;
        double tasaInteresMensual = 0.016;
        double pagoMensual = 291.67;  
        
        System.out.println("Saldo inicial: S/ 5000");
        System.out.println("Tasa de interés mensual: " + tasaInteresMensual*100 + "%");
        System.out.println("Pago mensual: S/ " + pagoMensual);
        
        for (int mes = 1; mes <= 18; mes++) {
            double interes = saldoPendiente * tasaInteresMensual;
            saldoPendiente += interes - pagoMensual;
            saldoPendiente = Math.round(saldoPendiente * 100.0) / 100.0; // Redondeo a dos decimales
            System.out.println("Saldo pendiente después del mes " + mes + ": S/ " + saldoPendiente);
        }
    }
}
