import java.util.Scanner;

public class EJE_CS_11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double monto = 10;
        double total = 0;
        int plazo = 20;

        System.out.print("Ingrese el monto inicial: ");
        monto = sc.nextDouble();

        for (int i = 1; i <= plazo; i++) {
            total += monto;
            System.out.printf("Mes %d: S/%.2f\n", i, monto);
            monto *= 2;
        }

        double pagoMensual = total / plazo;
        System.out.printf("Pago mensual: S/%.2f\n", pagoMensual);
        System.out.printf("Total a pagar después de %d meses: S/%.2f\n", plazo, total);
    }
}

