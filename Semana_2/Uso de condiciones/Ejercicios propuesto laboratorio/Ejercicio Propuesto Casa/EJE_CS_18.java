import java.util.Scanner;

public class EJE_CS_18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int limiteInferior, limiteSuperior, numero;
        int sumaDentro = 0, cantidadFuera = 0;
        boolean limiteInferiorEncontrado = false, limiteSuperiorEncontrado = false;
        do {
            System.out.print("Introduce el límite inferior del intervalo: ");
            limiteInferior = sc.nextInt();
            System.out.print("Introduce el límite superior del intervalo: ");
            limiteSuperior = sc.nextInt();
            if (limiteInferior > limiteSuperior) {
                System.out.println("El límite inferior debe ser menor o igual al límite superior.");
            }
        } while (limiteInferior > limiteSuperior);
        do {
            System.out.print("Introduce un número (0 para terminar): ");
            numero = sc.nextInt();
            if (numero == limiteInferior) {
                limiteInferiorEncontrado = true;
            }
            if (numero == limiteSuperior) {
                limiteSuperiorEncontrado = true;
            }
            if (numero > limiteInferior && numero < limiteSuperior) {
                sumaDentro += numero;
            } else {
                cantidadFuera++;
            }
        } while (numero != 0);
        System.out.println("La suma de los números dentro del intervalo es: " + sumaDentro);
        System.out.println("La cantidad de números fuera del intervalo es: " + cantidadFuera);
        if (limiteInferiorEncontrado) {
            System.out.println("Se ha introducido el límite inferior del intervalo.");
        }
        if (limiteSuperiorEncontrado) {
            System.out.println("Se ha introducido el límite superior del intervalo.");
        }
    }
}
