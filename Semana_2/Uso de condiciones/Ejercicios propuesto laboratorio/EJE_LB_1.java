/*Escribir un programa que imprima la secuencia de Fibonacci 
hasta un número específico, utilizando un bucle while.
 */
import java. util.Scanner;

public class EJE_LB_1 {
    public static void main(String[] args) {
        Scanner sn = new Scanner(System.in);
        System.out.println("ingrese un numero: ");
        int num = sn.nextInt();
        
        int a = 0 , b = 1,c;
        System.out.println("SECUENCIA ES : ");
        while (a <= num ){
            System.out.println( a + " ");
            c = a + b;
            a = b;
            b = c;

        }

    }
}