class Matris {
    public static void main(String[] args) {
        int[][] matriz = new int[7][7];
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                matriz[i][j] = (int) (Math.random() * 10);
            }
        }
        System.out.println("Matriz original:");
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                System.out.print(matriz[i][j] + " ");
            }
            System.out.println();
        }
        int[][] matrizRotada = new int[7][7];
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                matrizRotada[i][j] = matriz[j][6-i];
            }
        }
        System.out.println("Matriz rotada 90 grados en sentido horario:");
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                System.out.print(matrizRotada[i][j] + " ");
            }
            System.out.println();
        }
    }
}
