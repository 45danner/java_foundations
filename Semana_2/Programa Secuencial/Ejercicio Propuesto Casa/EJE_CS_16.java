import java.util.HashMap;
class repite {
    public static int findMostFrequent(int[] arr) {
        HashMap<Integer, Integer> count = new HashMap<Integer, Integer>();
        int mostFrequent = arr[0];
        int maxCount = 1;
        for (int i = 0; i < arr.length; i++) {
            int num = arr[i];
            if (count.containsKey(num)) {
                int newCount = count.get(num) + 1;
                count.put(num, newCount);
                if (newCount > maxCount) {
                    mostFrequent = num;
                    maxCount = newCount;
                }
            } else {
                count.put(num, 1);
            }
        }
        return mostFrequent;
    }

    public static void main(String[] args) {
        int[] arr = { 1, 2, 3, 4, 5, 3, 3, 3, 2, 2, 2, 2 };
        int mostFrequent = findMostFrequent(arr);
        System.out.println("El número que se repite más veces es: " + mostFrequent);
    }
}
