import java.util.Arrays;

class subarray {

    public static void main(String[] args) {
        int[] arr = {5, 6, 3, 4, 8, 7, 2, 9};
        int[] lis = findLIS(arr);
        System.out.println("Subarreglo de longitud máxima en orden creciente: " + Arrays.toString(lis));
    }

    public static int[] findLIS(int[] arr) {
        int n = arr.length;
        int[] lis = new int[n];
        int[] prev = new int[n];

        for (int i = 0; i < n; i++) {
            lis[i] = 1;
            prev[i] = -1;

            for (int j = 0; j < i; j++) {
                if (arr[j] < arr[i] && lis[j] + 1 > lis[i]) {
                    lis[i] = lis[j] + 1;
                    prev[i] = j;
                }
            }
        }

        int maxIdx = 0;
        for (int i = 0; i < n; i++) {
            if (lis[i] > lis[maxIdx]) {
                maxIdx = i;
            }
        }

        int[] result = new int[lis[maxIdx]];
        int idx = lis[maxIdx] - 1;
        while (maxIdx != -1) {
            result[idx] = arr[maxIdx];
            maxIdx = prev[maxIdx];
            idx--;
        }

        return result;
    }
}
