

 class SumaParejas {
    public static void main(String[] args) {
        int[] nums = {1, 2, 3, 4, 5};
        int count = 0;

        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                int sum = nums[i] + nums[j];
                if (isPrime(sum)) {
                    count++;
                }
            }
        }

        System.out.println("Número de parejas cuya suma es un número primo: " + count);
    }

    public static boolean isPrime(int n) {
        if (n <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(n); i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}
