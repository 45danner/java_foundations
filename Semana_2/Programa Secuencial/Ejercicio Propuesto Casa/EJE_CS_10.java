import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

class Moda {

    public static int encontrarModa(int[] arreglo) {
        Arrays.sort(arreglo); 
        Map<Integer, Integer> frecuencia = new HashMap<>();
        for (int num : arreglo) {
            if (frecuencia.containsKey(num)) {
                frecuencia.put(num, frecuencia.get(num) + 1);
            } else {
                frecuencia.put(num, 1);
            }
        }
        int moda = arreglo[0];
        int maxFrecuencia = 1;
        for (Map.Entry<Integer, Integer> entry : frecuencia.entrySet()) {
            int num = entry.getKey();
            int freq = entry.getValue();
            if (freq > maxFrecuencia) {
                moda = num;
                maxFrecuencia = freq;
            }
        }
        return moda;
    }
    public static void main(String[] args) {
        int[] arreglo = {1, 2, 3, 4, 5, 6, 2, 2, 2, 7, 7, 8};
        int moda = encontrarModa(arreglo);
        System.out.println("La moda del arreglo es: " + moda);
    }
}
