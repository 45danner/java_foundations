import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.DecompositionSolver;
import org.apache.commons.math3.linear.EigenDecomposition;
import org.apache.commons.math3.linear.RealMatrix;

class Jordan {
    public static void main(String[] args) {
        RealMatrix matrix = getRandomMatrix(7, 7, 100);       
        System.out.println("Matriz original:");
        printMatrix(matrix);
        
        EigenDecomposition eigen = new EigenDecomposition(matrix);
        RealMatrix D = eigen.getD();    
        RealMatrix V = eigen.getV(); 
        DecompositionSolver solver = new LUDecomposition(V).getSolver();
        RealMatrix P = solver.solve(new Array2DRowRealMatrix(new double[7][7]));
        
        RealMatrix J = P.multiply(matrix).multiply(P.inverse());
        
        System.out.println("Matriz en forma canónica de Jordan:");
        printMatrix(J);
    }
        public static void printMatrix(RealMatrix matrix) {
        for (int i = 0; i < matrix.getRowDimension(); i++) {
            for (int j = 0; j < matrix.getColumnDimension(); j++) {
                System.out.print(matrix.getEntry(i, j) + "\t");
            }
            System.out.println();
        }
    }
        public static RealMatrix getRandomMatrix(int n, int m, int max) {
        RealMatrix matrix = new Array2DRowRealMatrix(n, m);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                matrix.setEntry(i, j, (int) (Math.random() * max) + 1);
            }
        }
        return matrix;
    }
}
