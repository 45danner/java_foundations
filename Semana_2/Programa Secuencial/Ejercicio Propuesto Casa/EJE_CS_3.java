class Multiplicacion {
    public static void main(String[] args) {
        int[][] matriz1 = new int[5][5];
        int[][] matriz2 = new int[3][3];
        for (int i = 0; i < matriz1.length; i++) {
            for (int j = 0; j < matriz1[0].length; j++) {
                matriz1[i][j] = (int) (Math.random() * 100 + 1);
            }
        }
        for (int i = 0; i < matriz2.length; i++) {
            for (int j = 0; j < matriz2[0].length; j++) {
                matriz2[i][j] = (int) (Math.random() * 100 + 1);
            }
        }
        System.out.println("Matriz1:");
        for (int i = 0; i < matriz1.length; i++) {
            for (int j = 0; j < matriz1[0].length; j++) {
                System.out.print(matriz1[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println("Matriz2:");
        for (int i = 0; i < matriz2.length; i++) {
            for (int j = 0; j < matriz2[0].length; j++) {
                System.out.print(matriz2[i][j] + "\t");
            }
            System.out.println();
        }
        int[][] resultado = new int[5][3];
        for (int j = 0; j < matriz2[0].length; j++) {
            for (int i = 0; i < matriz1.length; i++) {
                int sum = 0;
                for (int k = 0; k < matriz1[0].length; k++) {
                    sum += matriz1[i][k] * matriz2[k][j];
                }
                resultado[i][j] = sum;
            }
        }
        System.out.println("Resultado:");
        for (int i = 0; i < resultado.length; i++) {
            for (int j = 0; j < resultado[0].length; j++) {
                System.out.print(resultado[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
