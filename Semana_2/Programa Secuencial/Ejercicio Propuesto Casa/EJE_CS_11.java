class Mean {
    public static double calculateMean(int[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }
        return (double) sum / arr.length;
    }

    public static void main(String[] args) {
        int[] arr = {3, 5, 2, 8, 1};
        double mean = calculateMean(arr);
        System.out.println("La media del arreglo es: " + mean);
    }
}
