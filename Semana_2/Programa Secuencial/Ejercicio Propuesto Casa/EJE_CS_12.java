class largo {
    public static void main(String[] args) {
        int[] nums = {5, 10, 2, 8, 3};
        int max = nums[0];
        for(int i = 1; i < nums.length; i++) {
            if(nums[i] > max) {
                max = nums[i];
            }
        }
        System.out.println("El número más grande es: " + max);
    }
}
