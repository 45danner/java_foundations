class Division {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8};
        int n = arr.length;
        int mid = n / 2;
        int[] arr1 = new int[mid];
        int[] arr2 = new int[n - mid];

        for (int i = 0; i < mid; i++) {
            arr1[i] = arr[i];
        }

        for (int i = mid; i < n; i++) {
            arr2[i - mid] = arr[i];
        }

        System.out.println("Array original: " + Arrays.toString(arr));
        System.out.println("Primer array: " + Arrays.toString(arr1));
        System.out.println("Segundo array: " + Arrays.toString(arr2));
    }
}
