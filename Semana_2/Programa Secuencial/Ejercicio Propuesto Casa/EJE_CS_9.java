import java.util.Arrays;

class Median {

    public static double findMedian(int[] nums) {
        Arrays.sort(nums);
        int n = nums.length;
        if (n % 2 == 0) {
            int mid = n / 2;
            return (nums[mid - 1] + nums[mid]) / 2.0;
        } else {
            return nums[n / 2];
        }
    }
    public static void main(String[] args) {
        int[] nums = {1, 2, 3, 4, 5};
        double median = findMedian(nums);
        System.out.println("La mediana del arreglo es: " + median);
    }
}
