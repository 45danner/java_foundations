import java.util.Arrays;

class Count {

    public static int countOccurrences(int[] arr, int target) {
        int index = Arrays.binarySearch(arr, target); // Realiza la búsqueda binaria en el arreglo
        if (index < 0) { 
            return 0;
        }
        int count = 1;
        for (int i = index - 1; i >= 0; i--) {
            if (arr[i] == target) {
                count++;
            } else {
                break;
            }
        }
        for (int i = index + 1; i < arr.length; i++) {
            if (arr[i] == target) {
                count++;
            } else {
                break;
            }
        }
        return count;
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 2, 3, 4, 4, 4, 5, 5, 5, 5};
        int target = 4;
        int occurrences = countOccurrences(arr, target);
        System.out.println("El número " + target + " aparece " + occurrences + " veces en el arreglo.");
    }
}
