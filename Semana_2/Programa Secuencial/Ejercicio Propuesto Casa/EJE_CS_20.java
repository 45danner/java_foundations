class Array {
    public static void main(String[] args) {
        int[] asciiArray = new int[256];
        for (int i = 0; i < 256; i++) {
            asciiArray[i] = i;
        }
        char[] charArray = new char[256];
        for (int i = 0; i < 256; i++) {
            charArray[i] = (char) asciiArray[i];
        }
        System.out.println(charArray);
    }
}
