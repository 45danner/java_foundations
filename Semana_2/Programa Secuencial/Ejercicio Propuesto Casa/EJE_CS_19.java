import java.util.Scanner;

class letras {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Ingrese una letra: ");
        char letter = input.next().toLowerCase().charAt(0);
        
        if (letter == 'a' || letter == 'e' || letter == 'i' || letter == 'o' || letter == 'u') {
            System.out.println(letter + " es una vocal.");
        } else {
            System.out.println(letter + " es una consonante.");
        }
    }
}
