import java.util.Arrays;
import java.util.Comparator;

class Inverso {
    public static void main(String[] args) {
        String[] cadenas = {"hola", "danner", "java", "programacion"};
        
        Arrays.sort(cadenas, Comparator.reverseOrder());
        
        System.out.println(Arrays.toString(cadenas));
    }
}
