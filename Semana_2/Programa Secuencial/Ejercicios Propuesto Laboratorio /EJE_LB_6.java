/*Crea un programa que pregunte al usuario si está lloviendo, en caso de responder “sí” pregunta si 
está haciendo mucho viento y si responde “sí” nuevamente muestra un mensaje indicando que hace mucho viento para salir 
con una sombrilla. En caso contrario, anima al usuario a que lleve una sombrilla. Para el caso de responder “no” en la primer pregunta, entonces solo desea un bonito día.
Considera que las respuestas pueden pasarse a 
minúsculas para evitar posibles errores y que los resultados se almacenen en un array. */
import java.util.Scanner;

class lluvia {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        String[] respuestas = new String[2];

        System.out.println("¿Está lloviendo? (si/no)");
        respuestas[0] = scanner.nextLine().toLowerCase();

        if (respuestas[0].equals("si")) {
            System.out.println("¿Está haciendo mucho viento? (sí/no)");
            respuestas[1] = scanner.nextLine().toLowerCase();
            if (respuestas[1].equals("sí")) {
                mensajeViento();
            } else {
                mensajeSombrilla();
            }
        } else {
            mensajeBonitoDia();
        }
    }

    private static void mensajeViento() {
        System.out.println("¡Hace mucho viento para salir con una sombrilla!");
    }

    private static void mensajeSombrilla() {
        System.out.println("Lleva una sombrilla contigo.");
    }

    private static void mensajeBonitoDia() {
        System.out.println("¡Que tengas un bonito día!");
    }
}
