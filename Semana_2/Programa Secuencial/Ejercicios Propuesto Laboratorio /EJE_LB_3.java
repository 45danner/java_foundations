/*Crear un método que calcule la suma de los elementos de un arreglo de enteros.
 */
import java.util.Scanner;

class calculando {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el tamaño del arreglo: ");
        int tam = sc.nextInt();
        
        int[] miArreglo = new int[tam];
        for (int i = 0; i < tam; i++) {
            System.out.print("Ingrese el elemento " + (i+1) + ": ");
            miArreglo[i] = sc.nextInt();
        }
        
        int suma = sumarArreglo(miArreglo);
        System.out.println("La suma de los elementos del arreglo es: " + suma);
    }

    public static int sumarArreglo(int[] arreglo) {
        int suma = 0;
        for (int i = 0; i < arreglo.length; i++) {
            suma += arreglo[i];
        }
        return suma;
    }
}
