/*Queremos guardar los nombres y la edades de los alumnos de un curso. Realiza un programa que introduzca el nombre y la edad de cada alumno. El proceso de lectura de datos terminará cuando se introduzca como nombre un asterisco (*) Al finalizar se mostrará los siguientes datos:
Todos lo alumnos mayores de edad.
Los alumnos mayores (los que tienen más edad) */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

class AlumnosCurso {
    private ArrayList<String> nombres;
    private ArrayList<Integer> edades;

    public AlumnosCurso() {
        nombres = new ArrayList<>();
        edades = new ArrayList<>();
    }

    public void agregarAlumno(String nombre, int edad) {
        nombres.add(nombre);
        edades.add(edad);
    }

    public void mostrarMayoresEdad() {
        System.out.println("Alumnos mayores de edad:");
        for (int i = 0; i < nombres.size(); i++) {
            if (edades.get(i) >= 18) {
                System.out.println(nombres.get(i) + " - " + edades.get(i) + " años");
            }
        }
    }

    public void mostrarAlumnoMayor() {
        int posMayor = edades.indexOf(Collections.max(edades));
        System.out.println("Alumno mayor edad es : " + nombres.get(posMayor) + "Edad " + edades.get(posMayor) + " años");
    }

    public static void main(String[] args) {
        Scanner sn = new Scanner(System.in);
        AlumnosCurso curso = new AlumnosCurso();
        String nombre;
        int edad;

        do {
            System.out.print("Introduce el nombre del alumno: ");
            nombre = sn.nextLine();
            if (!nombre.equals("*")) {
                System.out.print("Introduce la edad de " + nombre + ": ");
                edad = sn.nextInt();
                sn.nextLine(); 
                curso.agregarAlumno(nombre, edad);
            }
        } while (!nombre.equals("*"));

        curso.mostrarMayoresEdad();
        curso.mostrarAlumnoMayor();
    }
}
/*DNR_DANNER-PEREZ */