/*Crear un programa para el que el usuario indique un número entre 1 y 7. Como respuesta se le brindará un mensaje según el número leido:
1 - “Hoy aprenderemos sobre prorgamación”
2 - “¿Qué tal tomar un curso de marketing digital?
3 - “Hoy es un gran día para comenzar a aprender de diseño”
4 - ¿Y si aprendemos algo de negocios online?
5 - “Veamos un par de clases sobre producción audiovisual”
6 - “Tal vez sea bueno desarrollar una habilidad blanda”
7 - “Yo decido distraerme programando”
En caso indicar un número distinto, pedir al usuario que ingrese uno en el rango válido. */
import java.util.Scanner;

class Dia {
    void menu(int opcion) {
        switch (opcion) {
            case 1:
                System.out.println("Hoy aprenderemos sobre programación");
                break;
            case 2:
                System.out.println("¿Qué tal tomar un curso de marketing digital?");
                break;
            case 3:
                System.out.println("Hoy es un gran día para comenzar a aprender de diseño");
                break;
            case 4:
                System.out.println("¿Y si aprendemos algo de negocios online?");
                break;
            case 5:
                System.out.println("Veamos un par de clases sobre producción audiovisual");
                break;
            case 6:
                System.out.println("Tal vez sea bueno desarrollar una habilidad blanda");
                break;
            case 7:
                System.out.println("Yo decido distraerme programando");
                break;
            default:
                System.out.println("Humano elija bien ");
                break;
        }
    }

    public static void main(String[] args) {
        Scanner sn = new Scanner(System.in);
        System.out.println("Ingrese un número del 1 al 7: ");
        int numero = sn.nextInt();

        Dia seleccion = new Dia();
        seleccion.menu(numero);
    }
}
