/* Crear un método que encuentre el elemento máximo en un arreglo de enteros.
*/

class buscando {
    
    
    public static int encontrarMaximo(int[] arreglo) {
        int maximo = arreglo[0];
        for(int i=1; i<arreglo.length; i++) {
            if(arreglo[i] > maximo) {
                maximo = arreglo[i];
            }
        }
        return maximo;
    }
    public static void main(String[] args) {
        int[] arreglo = {4, 7, 2, 9, 1, 5, 8, 3, 6,345};
        int maximo = encontrarMaximo(arreglo);
        System.out.println("El elemento máximo del arreglo es: " + maximo);
    }
    

    
}