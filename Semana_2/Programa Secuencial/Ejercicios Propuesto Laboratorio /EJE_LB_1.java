/* Desarrollar un programa que permita almacenar los datos de los estudiantes de Ing. del Software de SENATI en un areglo bidimencional, donde los datos son nombres y edad, el programa deberá calcular la edad promedio de los estuadiantes,
 el programa de hacer uso de métodos.*/
 import java.util.Scanner;

class Estudiantes {
 
     private static Scanner sc = new Scanner(System.in);
 
     public static void main(String[] args) {
         int numEstudiantes = obtenerNumeroEstudiantes();
 
         String[][] estudiantes = obtenerDatosEstudiantes(numEstudiantes);
 
         imprimirEstudiantes(estudiantes);
     }
 
     private static int obtenerNumeroEstudiantes() {
         System.out.print("Ingrese el número de estudiantes: ");
         return sc.nextInt();
     }
 
     private static String[][] obtenerDatosEstudiantes(int numEstudiantes) {
         String[][] estudiantes = new String[numEstudiantes][2];
 
         for (int i = 0; i < numEstudiantes; i++) {
             System.out.println("Ingrese los datos del estudiante " + (i + 1));
             System.out.print("Nombre: ");
             estudiantes[i][0] = sc.next();
             System.out.print("Edad: ");
             estudiantes[i][1] = sc.next();
         }
 
         return estudiantes;
     }
 
     private static void imprimirEstudiantes(String[][] estudiantes) {
         System.out.println("\nLos estudiantes de Ing. de Software son:\n");
 
         for (int i = 0; i < estudiantes.length; i++) {
             System.out.println("Nombre: " + estudiantes[i][0] + ", Edad: " + estudiantes[i][1]);
         }
     }
 }
 