/*Leer un numero entre 1000 a 7000 y almacenar los digitos en un array e imprimirlos en pantalla de la siguiente manera:


Ingresa el número: 5501
Salida:
[3] = 5
[2] = 5
[1] = 0
[0] = 1 */

import java.util.Scanner;

 class leer {
    public static void main(String[] args) {
        int num = leerNumero();

        int[] digitos = obtenerDigitos(num);

        System.out.println("Salida:");
        for (int j = digitos.length - 1; j >= 0; j--) {
            System.out.println("[" + j + "] = " + digitos[j]);
        }
    }

    public static int leerNumero() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingresa el número: ");
        return sc.nextInt();
    }

    public static int[] obtenerDigitos(int num) {
        int[] digitos = new int[4];
        int i = 0;

        while (num > 0) {
            int digito = num % 10;
            digitos[i] = digito;
            i++;
            num = num / 10;
        }

        return digitos;
    }
}
