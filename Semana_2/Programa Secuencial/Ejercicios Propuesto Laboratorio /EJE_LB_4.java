/*Crear un método que ordene un arreglo de enteros de manera descendente. */

import java.util.Scanner;

 class OrdenamientoDescendente {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el tamaño del arreglo: ");
        int tam = sc.nextInt();

        int[] miArreglo = new int[tam];
        for (int i = 0; i < tam; i++) {
            System.out.print("Ingrese el elemento " + (i+1) + ": ");
            miArreglo[i] = sc.nextInt();
        }

        ordenarDescendente(miArreglo);

        System.out.print("El arreglo ordenado de manera descendente es: ");
        for (int i = 0; i < miArreglo.length; i++) {
            System.out.print(miArreglo[i] + " ");
        }
    }

    public static void ordenarDescendente(int[] arreglo) {
        int temp;
        for (int i = 0; i < arreglo.length; i++) {
            for (int j = i+1; j < arreglo.length; j++) {
                if (arreglo[i] < arreglo[j]) {
                    temp = arreglo[i];
                    arreglo[i] = arreglo[j];
                    arreglo[j] = temp;
                }
            }
        }
    }
}
